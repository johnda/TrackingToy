import numpy as np
from simpleLayer import simpleLayer
from simpleDisk import simpleDisk
import matplotlib.pyplot as plt
from TrackHelper import getPhiCircle
from TrackHelper import getSSIDS
import pandas as pd

class simpleDetectorGeo: 
    
    def __init__(self, rDet, zDet = None, zDet_disk = None, rDetStart_disk = None, rDetEnd_disk = None ):
        self.layers = []
        self.disks  = []
        
        if zDet is None:
            for layerID, layerR  in enumerate(rDet):
                self.layers.append(simpleLayer(layerID,rDet[layerID]))
        else:
            for layItr in range(len(rDet)):
                self.layers.append(simpleLayer(layItr,rDet[layItr],zDet[layItr]))

        if not zDet_disk is None:
            for diskItr in range(len(zDet_disk)):
                self.disks.append(simpleDisk(   (diskItr+1),   zDet_disk[diskItr],rDetStart_disk[layItr],rDetEnd_disk[layItr]))
                self.disks.append(simpleDisk(-1*(diskItr+1),-1*zDet_disk[diskItr],rDetStart_disk[layItr],rDetEnd_disk[layItr]))

    # For the FTK-like processing
    #   Probably want to make these add tower to list of all towers

    def makeTower(self, phiCenter, phiWidth, nSS):
        self.towerPhiCenter = phiCenter
        self.towerPhiWidth = phiWidth
        for layItr, l in enumerate(self.layers): 
            l.makeTower(phiCenter, phiWidth, nSS[layItr])

    def drawDetector(self,ax,detailed=False):
        for l in self.layers: l.drawLayer(ax, detailed)
    
    def drawDetectorXY(self,ax):
        for l in self.layers: l.drawDetectorXY(ax)

    def drawDetectorZR(self,ax):
        for l in self.layers: l.drawDetectorZR(ax)
        for d in self.disks:  d.drawDetectorZR(ax)

    # Takes a list of SSIDs  of the form (layer, SSIDS)  to be drawn
    def drawSSIDs(self,ax,SSIDList):
        for ssid in SSIDList: 
            layer = int(ssid[0])
            self.layers[layer].drawSSID(ax,ssid[1])

    def drawTower(self, ax, detailed=False):
        # Drew the wedge
        r = self.layers[-1].layerR
        phi1 = self.towerPhiCenter + self.towerPhiWidth/2
        phi2 = self.towerPhiCenter - self.towerPhiWidth/2
        plt.plot([np.cos(phi2)*r,0,np.cos(phi1)*r],[np.sin(phi2)*r,0,np.sin(phi1)*r],'--')

        # Dray the layers
        for l in self.layers: l.drawTower(ax, detailed)
    
    def getHits(self,trk_rc,trk_phi,trk_q, tol=0.01):
        # Convert 0-2Pi
        if trk_phi<0: trk_phi += 2*np.pi
        trk_phi_circle = getPhiCircle(trk_phi,trk_q)
        trk_xc = trk_rc*np.cos(trk_phi_circle)
        trk_yc = trk_rc*np.sin(trk_phi_circle)

        # Global positions of the hits 
        #   pseudo Hits are the locations of the hits in the fully phi-symetric geometry

        xs = []
        ys = []
        layers = np.array([])        
        becs = np.array([])        
        
        for l in self.layers:
            x, y, layer, bec = l.getHits(trk_xc,trk_yc, trk_phi, trk_q)
            
            xs.append(x)
            ys.append(y)
            layers = np.append(layers,layer)                        
            becs   = np.append(becs,bec)                    
    
        return xs, ys, layers, becs

    def getHits3D(self,trk_rc,trk_phi,trk_theta, trk_z, trk_q, tol=0.01):
        # Convert 0-2Pi                                                                                                                                   
        if trk_phi<0: trk_phi += 2*np.pi
        trk_phi_circle = getPhiCircle(trk_phi,trk_q)
        trk_xc = trk_rc*np.cos(trk_phi_circle)
        trk_yc = trk_rc*np.sin(trk_phi_circle)

        # Global positions of the hits                                                                                                                    
        #   pseudo Hits are the locations of the hits in the fully phi-symetric geometry                                                                  
        xs = np.array([])
        ys = np.array([])
        zs = np.array([])
        rs = np.array([])        
        layers = np.array([])        
        becs = np.array([])        

        for l in self.layers:
            x, y, z, r, layer, bec  = l.getHits3D(trk_xc,trk_yc, trk_phi, trk_theta, trk_z, trk_q)
            if not x is None:
                xs     = np.append(xs,x)
                ys     = np.append(ys,y)
                zs     = np.append(zs,z)            
                rs     = np.append(rs,r)                        
                layers = np.append(layers,layer)                        
                becs   = np.append(becs,bec)                        

        for d in self.disks:
            x, y, z, r, layer, bec  = d.getHits3D(trk_xc,trk_yc, trk_phi, trk_theta, trk_z, trk_q)
            if not x is None:
                xs = np.append(xs,x)
                ys = np.append(ys,y)
                zs = np.append(zs,z)            
                rs = np.append(rs,r)                        
                layers = np.append(layers,layer)                        
                becs   = np.append(becs,bec)                        

        return xs, ys, zs, rs, layers, becs



    # Takes list of hits with format (hitID,hitX,hitY,layerID)
    # Returns a list with format     (hitID,hitX,hitY,layerID, SSID)
    def addSSIDs(self,hitList,filterOnTower=False):

        # One list per layer
        hitsAndSSIDS = []

        for h in hitList:
            hLayer = int(h[3])
            hX     = h[1]
            hY     = h[2]
            if filterOnTower and not self.layers[hLayer].hitInTower(hX,hY):
                continue
            hitsAndSSIDS.append( np.append(h,self.layers[hLayer].getSSID(hX,hY)) )

        return hitsAndSSIDS

    # Takes a list of hits and checks if they are all contained in the tower
    def hitsAllInTower(self,hitList, debug=False):
        for h in hitList:
            hLayer = h[0]
            hX     = h[1]
            hY     = h[2]
            if not self.layers[hLayer].hitInTower(hX,hY,debug):
                if debug: print("failed layer",hLayer)
                return False

        return True

    # 
    def DOWrite(self,hitsAndSSIDs):

        for l in self.layers:
            l.DOClear()

        for h in hitsAndSSIDs:
            hLayer = int(h[3])
            hID    = h[0] 
            hSSID  = h[4]
            self.layers[hLayer].DOWrite(hID,hSSID)


        #for l in self.layers:
        #    print(l.ssidToHit)

    def isPatternFired(self,p):

        # Check each layer 
        for layItr, l in enumerate(self.layers): 
            p_ssid = int((p / 100**layItr)) % 100

            if not l.isSSIDFired(p_ssid):
                return False
        
        return True


    def getSolutions(self, matchedPatterns, trkIDInit=0):

        evt_id = []
        hit_id = []
        trk_id = []

        trkID = trkIDInit

        for mp in matchedPatterns:
            trkID +=1 
            thisSSIDs = getSSIDS(mp)

            this_evt_id = []
            this_hit_id = []
            this_trk_id = []
            for laySSID in thisSSIDs:
                for hit in self.layers[laySSID[0]].ssidToHit[laySSID[1]]:
                    # Sub optimial !
                    if hit not in hit_id:
                        this_evt_id.append(0)
                        this_hit_id.append(hit)
                        this_trk_id.append(trkID)

            if len(this_hit_id) > 3:
                evt_id += this_evt_id
                hit_id += this_hit_id
                trk_id += this_trk_id
                    
    
        solnDict = {"event_id" : pd.Series(evt_id),
                    "hit_id" : pd.Series(hit_id),
                    "track_id" : pd.Series(trk_id),
                    }

        solns = pd.DataFrame(solnDict)
        return solns, (evt_id, hit_id, trk_id)
