import numpy as np
import matplotlib.pyplot as plt
from module import module
from TrackHelper import getHitsCylinder3D
from TrackHelper import getHitsCylinderXY
#from matplotlib.patches import Arc
#from math import floor

class disk:
    
    def __init__(self):
        self.mods = []


    def initFromFile(self,moduleData,nModulesInR):
        self.volume_id = moduleData.iloc[0]["volume_id"]
        self.layer_id  = moduleData.iloc[0]["layer_id"]

        self.RStart = 1e5
        self.REnd = 0
        self.nRMods = nModulesInR
        for i in range(len(moduleData)):
            self.mods.append(module())
            self.mods[-1].initFromFile(moduleData.iloc[i])
            
            if self.mods[-1].r_max > self.REnd:   self.REnd = self.mods[-1].r_max
            if self.mods[-1].r_min < self.RStart: self.RStart = self.mods[-1].r_min

            
        self.diskZ = (self.mods[0].z_min+self.mods[0].z_max)*0.5

        # For checking if the module is hit
        self.thetaEdges = [np.arctan2(self.RStart,self.diskZ),np.arctan2(self.REnd,self.diskZ)]
        self.minTheta   = min(self.thetaEdges)
        self.maxTheta   = max(self.thetaEdges)



    def drawDiskXY(self):
        for m in self.mods:
            m.drawEndcapModuleXY(color="gray")

    def drawDiskModulesXY(self, modID, detailed=False):
        for m in self.mods:
            if m.module_id == modID:
                m.drawEndcapModuleXY(color="r")


    def drawLayerZR(self,ax,detailed=False):

        xs = [self.diskZ,self.diskZ]
        ys_up = [self.RStart,self.REnd]
        plt.plot(xs,ys_up,"k",linestyle="--")


        #ys_down = [-self.layerR,-self.layerR]
        #plt.plot(xs,ys_down,"k",linestyle="--")
        
        for mItr in range(self.nRMods):
            self.mods[mItr].drawModuleZR("gray")

        #if detailed:
        #    for miter, m in enumerate(self.mods): 
        #        if miter%self.nPhiMods == 0:
        #            m.drawModuleZR(color="gray")


    def getHits3D(self,trk_xc,trk_yc,trk_phi,trk_theta, trk_z, trk_q, tol=0.01):
        # pseudo Hits are the locations of the hits in the fully phi-symetric geometry
        ps_Hitx, ps_Hity, ps_Hitz, ps_Hitr = self.getPseudoHits3D(trk_xc,trk_yc,trk_phi,trk_theta, trk_z, trk_q, tol)
        
        # check if layer is actually hit
        ps_theta = np.arctan2(ps_Hitr,ps_Hitz)
        if ps_theta < 0: ps_theta += 2*np.pi

        if (ps_theta < self.minTheta or ps_theta > self.maxTheta):
            return None,None,None,None,None,None

        ps_HitPhi = np.arctan2(float(ps_Hity),float(ps_Hitx))

        modulesHit = []
        
        real_Hits = []

        debug = False

        for m in self.mods:

            if m.isDiskModuleHit3D(ps_HitPhi,ps_theta,ps_Hitr,debug=debug):
                elementID = (self.volume_id,self.layer_id,m.module_id)
                modulesHit.append(elementID)

                hitx, hity, hitz = m.getDiskHits3D(trk_xc,trk_yc,trk_phi,trk_theta, trk_z, trk_q)

                if len(hitx): real_Hits.append((elementID,hitx[0], hity[0], hitz[0]))
        if debug: print(self.volume_id,self.layer_id,modulesHit)
                    
        return ps_Hitx, ps_Hity, ps_Hitz, ps_Hitr, modulesHit, real_Hits




    def getPseudoHits3D(self,trk_xc,trk_yc,trk_phi,trk_theta, trk_z,trk_q, tol=0.01,debug=False):
        if debug: print("getPseudoHits3D disk",self.volume_id,self.layer_id)
        
        # Start with r
        z_sol = self.diskZ
        slopeRZ = np.tan(trk_theta)
        r_sol = slopeRZ*(z_sol-trk_z)

        x_sol, y_sol = getHitsCylinderXY(r_sol,trk_xc,trk_yc,trk_phi,trk_q, tol,debug)

        return x_sol, y_sol, z_sol, r_sol
