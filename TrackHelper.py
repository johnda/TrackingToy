import numpy as np
import matplotlib.pyplot as plt
from math import ceil
import pandas as pd

# Note there are 2 different phis used:
#   - phi_circle (the angle of the center of the circle in r-phi plane)
#   - phi_track (the direction of the particle that made the track) 
#
#  For positive tracks: phi_circle = phi_track - pi/2
#  For negative tracks: phi_circle = phi_track + pi/2
def getPhiCircle(phi_track,q):
    if q > 0:
        return phi_track - np.pi/2
    elif q < 0:
        return phi_track + np.pi/2
    else:
        print("ERROR: q wrong",q)



def drawTrack(r,phi_track,q,scale=1):
    x = np.linspace(-3*scale, 3*scale, 5000)

    
    # Note there are 2 different phis used:
    #   - phi_circle (the angle of the center of the circle in r-phi plane)
    #   - phi_track (the direction of the particle that made the track) 
    #
    #  For positive tracks: phi_circle = phi_track - pi/2
    #  For negative tracks: phi_circle = phi_track + pi/2
    phi_circle = getPhiCircle(phi_track, q)
    if q > 0:  color = "r"
    else:      color= "b"

    xc = r*np.cos(phi_circle)
    yc = r*np.sin(phi_circle)

    yp = yc+np.sqrt(r**2-(x-xc)**2)
    plt.plot(x,yp,color)
    ym = yc-np.sqrt(r**2-(x-xc)**2)
    plt.plot(x,ym,color)


def drawTrackXY(r,phi_track,q,scale=1):
    drawTrack(r,phi_track,q,scale)

def drawTrackZR(theta,z0,q,zMax):
    slopeRZ = np.tan(theta)
    xs = [z0,zMax]
    ys = [0,slopeRZ*zMax-slopeRZ*z0]
    if q > 0:  color = "r"
    else:      color= "b"

    plt.plot(xs,ys,color)



# Finds the y-values of the intercetison of:
#   - a circle centered at the origin with radius rDet
#     and 
#   - a cricle that contains the origin with center at (xc, yc)
def solveLayer(xc, yc, rDet):
    A = xc*xc + yc*yc
    rDet2 = rDet*rDet
    B = -rDet2*yc
    C = rDet2*(rDet2/4-xc*xc)
    coeff = [A,B,C]
    yints = np.roots(coeff)
    return yints


#
# Convert from a patter to list of SSIDS
#
def getSSIDS(inPattern,nLay=6):
    outSSIDS = []
    for layItr in range(nLay):
        outSSIDS.append((layItr,int((inPattern % 100**(layItr+1) / 100**layItr))))
        #outSSIDS.append((layItr,int((inPattern / 100**layItr)) % 100))
    return outSSIDS





# Convert a list of SSIDs to a patterID
def patternID(ssidList,nSSperLayer):
    patternID = 0
    offset = 1
    for layItr, ssid in enumerate(ssidList):
        patternID += offset*ssid[0]
        offset    *= 10**ceil(np.log10(nSSperLayer[layItr]))
    return patternID




def getTruthDF(eventTruth):
    #        eventTruth.append((hitID,trkI,xHits[hitItr],yHits[hitItr],1.0))
    truthArr = np.array(eventTruth)
    truthArr = np.delete(truthArr, 2, axis=1)
    truthArr = np.delete(truthArr, 2, axis=1)
    truthArr = np.delete(truthArr, 2, axis=1)
    truthArr = np.array(truthArr,dtype=float)

    truthDict = {
        "hit_id" : pd.Series(truthArr[:,0]),
        "particle_id" : pd.Series(truthArr[:,1]),    
        "weight" : pd.Series(truthArr[:,2]),
    }

    truth = pd.DataFrame(truthDict)
    return truth


# Get XY position of Hits on infinite cylinder
def getHitsCylinderXY(cylR,trk_xc,trk_yc,trk_phi,trk_q, tol=0.01,debug=False):


    trk_rc2 = trk_xc**2  + trk_yc**2
    
    yints_raw = solveLayer(trk_xc, trk_yc, cylR)
    yints_raw = np.real(yints_raw)

    # logic to get the "right" solution (x-axis)
    # assume positive solution
    xints_p_raw =  1*np.sqrt(cylR**2-yints_raw**2)        
    xints_m_raw = -1*np.sqrt(cylR**2-yints_raw**2)
    
    if debug:
        print("y_raw",yints_raw)
        print("x_p",xints_p_raw,"==> dis:",trk_rc2-(xints_p_raw-trk_xc)**2)
        print("x_m",xints_m_raw,"==> dis:",trk_rc2-(xints_m_raw-trk_xc)**2)

    closure_raw_pp = abs(yints_raw-trk_yc+np.sqrt(trk_rc2-(xints_p_raw-trk_xc)**2))
    closure_raw_pm = abs(yints_raw-trk_yc-np.sqrt(trk_rc2-(xints_p_raw-trk_xc)**2))
    closure_raw_mp = abs(yints_raw-trk_yc+np.sqrt(trk_rc2-(xints_m_raw-trk_xc)**2))
    closure_raw_mm = abs(yints_raw-trk_yc-np.sqrt(trk_rc2-(xints_m_raw-trk_xc)**2))

    if debug:
        print("closure_raw_pp",closure_raw_pp)
        print("closure_raw_pm",closure_raw_pm)
        print("closure_raw_mp",closure_raw_mp)
        print("closure_raw_mm",closure_raw_mm)


    x_sol = 0
    y_sol = 0 
    phiDiff = 100

    for closure_list in [closure_raw_pp,closure_raw_pm]:
        for i, thisClosure in enumerate(closure_list): 
            if np.isnan(thisClosure): continue
            if debug: print("\t",thisClosure)
            if  thisClosure < tol: 
                this_xsol = xints_p_raw[i] 
                this_ysol = yints_raw[i]
                this_phisol = np.arctan2(this_ysol,this_xsol)
                this_phiDiff = abs(this_phisol-trk_phi)
                if this_phiDiff >np.pi: this_phiDiff = abs(this_phiDiff-2*np.pi)
                if debug: print("sols: x=",this_xsol,"y=",this_ysol,"phiDiff:",this_phiDiff)
                if this_phiDiff < phiDiff:
                    x_sol   = this_xsol
                    y_sol   = this_ysol
                    phiDiff = this_phiDiff

    for closure_list in [closure_raw_mp,closure_raw_mm]:
        for i, thisClosure in enumerate(closure_list): 
            if np.isnan(thisClosure): continue
            if debug: print("\t",thisClosure)
            if  thisClosure < tol: 
                this_xsol = xints_m_raw[i] 
                this_ysol = yints_raw[i]
                this_phisol = np.arctan2(this_ysol,this_xsol)
                this_phiDiff = abs(this_phisol-trk_phi)
                if this_phiDiff >np.pi: this_phiDiff = abs(this_phiDiff-2*np.pi)
                if debug: print("sols: x=",this_xsol,"y=",this_ysol,"phiDiff:",this_phiDiff)
                if this_phiDiff < phiDiff:
                    x_sol   = this_xsol
                    y_sol   = this_ysol
                    phiDiff = this_phiDiff

    if phiDiff > 2*np.pi:
        #print("getHitsCylinderXY::ERROR: no solution",phiDiff)
        return None, None
    
    return x_sol, y_sol



# Get 3D position of Hits on infinite cylinder
def getHitsCylinder3D(cylR,trk_xc,trk_yc,trk_phi,trk_theta, trk_z,trk_q, tol=0.01,debug=False):
        
    x_sol, y_sol  = getHitsCylinderXY(cylR,trk_xc,trk_yc,trk_phi,trk_q, tol,debug)

    if x_sol is None:
        return None,None,None,None

    #
    #  Now getting Z
    # 
    r_sol = np.sqrt(x_sol**2+ y_sol**2)
    slopeRZ = np.tan(trk_theta)
    interceptRZ = -1*slopeRZ*trk_z
    z_sol = (r_sol-interceptRZ)/slopeRZ
    
    
    return x_sol, y_sol, z_sol, r_sol
