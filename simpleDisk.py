import matplotlib.pyplot as plt
import numpy as np
from TrackHelper import getHitsCylinderXY

class simpleDisk:
    
    def __init__(self, diskID, diskZ, RStart, REnd):
        self.diskID  = diskID
        self.diskZ   = diskZ
        self.RStart  = RStart
        self.REnd    = REnd

        # For checking if the module is hit
        self.thetaEdges = [np.arctan2(self.RStart,self.diskZ),np.arctan2(self.REnd,self.diskZ)]
        self.minTheta   = min(self.thetaEdges)
        self.maxTheta   = max(self.thetaEdges)

    def drawDetectorZR(self,ax,detailed=False):

        xs = [self.diskZ,self.diskZ]
        ys_up = [self.RStart,self.REnd]
        plt.plot(xs,ys_up,"k",linestyle="--")

        ys_down = [-self.RStart,-self.REnd]
        plt.plot(xs,ys_down,"k",linestyle="--")


    def getHits3D(self,trk_xc,trk_yc,trk_phi,trk_theta, trk_z,trk_q, tol=0.01,debug=False):
        if debug: print("getHits3D layer",self.diskID)
        
        # Start with r
        z_sol = self.diskZ
        slopeRZ = np.tan(trk_theta)
        r_sol = slopeRZ*(z_sol-trk_z)

        x_sol, y_sol = getHitsCylinderXY(r_sol,trk_xc,trk_yc,trk_phi,trk_q, tol,debug)

        # check if layer is actually hit
        theta_sol = np.arctan2(r_sol,z_sol)
        if theta_sol < 0: theta_sol += 2*np.pi

        if (theta_sol < self.minTheta or theta_sol > self.maxTheta):
            return None,None,None,None,None,None


        return x_sol, y_sol, z_sol, r_sol, self.diskID, np.sign(self.diskID)
