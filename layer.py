import numpy as np
import matplotlib.pyplot as plt
from module import module
from TrackHelper import getHitsCylinderXY
from TrackHelper import getHitsCylinder3D
from matplotlib.patches import Arc
from math import floor

class layer:
    
    def __init__(self):
        self.mods = []
        

    def initSimple(self,layerID,layerR,nModules):
        self.volume_id = 0
        self.layer_id  = layerID
        self.layerR   = layerR
        for moduleID in range(nModules):
            self.mods.append(module())
            self.mods[-1].initSimple(moduleID,layerR, phiCenter = 2*moduleID/nModules * np.pi)


    def initFromFile(self,moduleData,nModulesInPhi):
        self.volume_id = moduleData.iloc[0]["volume_id"]
        self.layer_id  = moduleData.iloc[0]["layer_id"]
        
        for i in range(len(moduleData)):
            self.mods.append(module())
            self.mods[-1].initFromFile(moduleData.iloc[i])

        self.layerR = self.mods[0].r_ave
        self.nPhiMods = nModulesInPhi
        self.lengthZ = self.mods[-1].z_max

        


    def drawLayer(self,ax, detailed=False):
        thisLayer = plt.Circle((0, 0), self.layerR, color='k', linestyle="--",fill=False)
        ax.add_artist(thisLayer)
       
        if detailed:
            for m in self.mods: m.drawModuleXY(color="gray")

    def drawLayerXY(self,ax, detailed=False):
        thisLayer = plt.Circle((0, 0), self.layerR, color='k', linestyle="--",fill=False)
        ax.add_artist(thisLayer)
        
        if detailed:
            for miter, m in enumerate(self.mods): 
                if int(miter/self.nPhiMods) == 0:
                    m.drawModuleXY(color="gray")


    def drawLayerZR(self,ax,detailed=False):
        xs = [-self.lengthZ,self.lengthZ]
        ys_up = [self.layerR,self.layerR]
        plt.plot(xs,ys_up,"k",linestyle="--")

        ys_down = [-self.layerR,-self.layerR]
        plt.plot(xs,ys_down,"k",linestyle="--")
        
        if detailed:
            for miter, m in enumerate(self.mods): 
                if miter%self.nPhiMods == 0:
                    m.drawModuleZR(color="gray")



    def drawDetectorModules(self,modID, detailed=False):
        for m in self.mods:
            if m.module_id == modID:
                m.drawModuleXY(color="r", detailed=detailed)

    def drawDetectorModulesZR(self,modID, detailed=False):
        for m in self.mods:
            if m.module_id == modID:
                m.drawModuleZR(color="r", detailed=detailed)

                
    def getHits(self,trk_xc,trk_yc,trk_phi,trk_q, tol=0.01):
        # pseudo Hits are the locations of the hits in the fully phi-symetric geometry
        ps_Hitx, ps_Hity = self.getPseudoHits(trk_xc,trk_yc,trk_phi,trk_q, tol)
        
        ps_HitPhi = np.arctan2(float(ps_Hity),float(ps_Hitx))
        
        modulesHit = []
        
        real_Hits = []

        debug = False

        for m in self.mods:
            if m.isModuleHitXY(ps_HitPhi,debug=debug):
                elementID = (self.volume_id,self.layer_id,m.module_id)
                modulesHit.append(elementID)

                hitx, hity = m.getHits(trk_xc,trk_yc)

                if len(hitx): real_Hits.append((elementID,hitx[0], hity[0]))
        if debug: print(self.layer_id,modulesHit)
                    
        return ps_Hitx, ps_Hity, modulesHit, real_Hits
        
    # pseudo Hits are the locations of the hits in the fully phi-symetric geometry
    def getPseudoHits(self,trk_xc,trk_yc,trk_phi,trk_q, tol=0.01,debug=False):
        if debug: print("getPseudoHits layer",self.layer_id)
        x_sol, y_sol = getHitsCylinderXY(self.layerR, trk_xc,trk_yc,trk_phi,trk_q, tol,debug)
        
        return x_sol, y_sol


    # For the FTK-like processing
    #   Probably want to make these add tower to list of all towers
    def makeTower(self, phiCenter, phiWidth, nSS):
        self.towerPhiCenter = phiCenter
        if self.towerPhiCenter < 0: self.towerPhiCenter += 2*np.pi

        self.towerPhiWidth  = phiWidth
        self.towerPhiMin    = phiCenter - phiWidth/2
        self.towerPhiMax    = phiCenter + phiWidth/2
        if self.towerPhiMin < 0: self.towerPhiMin += 2*np.pi
        if self.towerPhiMax < 0: self.towerPhiMax += 2*np.pi
        
        self.nSS = nSS
        self.ssSizePhi = self.towerPhiWidth/self.nSS        

        self.towerModules = []
        for m in self.mods:
            if m.isInTower(phiCenter, phiWidth):
                self.towerModules.append(m)


    def drawTower(self,ax, detailed=False, drawPSLayer=True):
        if drawPSLayer:
            theta1=self.towerPhiMin*180./np.pi
            theta2=self.towerPhiMax*180./np.pi
            ax.add_patch(Arc((0, 0), 2*self.layerR, 2*self.layerR, theta1=theta1, theta2=theta2, edgecolor='k',linestyle="--"))

        for m in self.towerModules:
            m.drawModuleXY(color="gray")

        if detailed:
            for ssItr in range(self.nSS):
                ssPhiMin = self.towerPhiMin+ssItr*self.ssSizePhi
                ssPhiMax = self.towerPhiMin+(ssItr+1)*self.ssSizePhi
                ssPhiMin *= 180./np.pi
                ssPhiMax *= 180./np.pi

                color = "k" if (ssItr % 2 ==0 ) else "y"
                ax.add_patch(Arc((0, 0), 2*self.layerR, 2*self.layerR, theta1=ssPhiMin, theta2=ssPhiMax, edgecolor=color,linewidth=3))


    def hitInTower(self, hitX,hitY,debug=False):
        hitPhi = np.arctan2(hitY, hitX)   
        if hitPhi < 0: hitPhi += 2*np.pi
        
        aPhiDiff = abs(hitPhi-self.towerPhiCenter)
        if aPhiDiff > np.pi: aPhiDiff = 2*np.pi-aPhiDiff
        
        if aPhiDiff > self.towerPhiWidth/2: 
            return False

        return True


    def getSSID(self, hitX,hitY):
        hitPhi = np.arctan2(hitY, hitX)   
        if hitPhi < 0: hitPhi += 2*np.pi        

        localPhi = (hitPhi - self.towerPhiMin)
        if localPhi < -np.pi: localPhi += 2*np.pi

        # shouldnt happen
        if localPhi > self.towerPhiWidth: 
            print("simpleLayer::ERROR: localPhi (",round(localPhi,2),") bigger then towerPhiWidth (",round(self.towerPhiWidth,2),")", self.towerPhiMin,hitX,hitY)
            return -1
        if localPhi < 0:
            print("hitPhi ",hitPhi/np.pi,"pi", hitX,hitY,self.towerPhiMin, self.towerPhiMax)
            print("simpleLayer::ERROR: localPhi negative",localPhi, hitX,hitY,self.towerPhiMin)
            print(self.hitInTower(hitX,hitY))
            return -1
        
        return floor(localPhi/self.ssSizePhi)


    # Takes a list of SSIDs to be drawn
    def drawSSID(self,ax,ssID):
        for ssItr in range(self.nSS):
            if not ssItr == ssID : continue

            ssPhiMin = self.towerPhiMin+ssItr*self.ssSizePhi
            ssPhiMax = self.towerPhiMin+(ssItr+1)*self.ssSizePhi
            ssPhiMin *= 180./np.pi
            ssPhiMax *= 180./np.pi

            color = "r"
            ax.add_patch(Arc((0, 0), 2*self.layerR, 2*self.layerR, theta1=ssPhiMin, theta2=ssPhiMax, edgecolor=color, linewidth=5))



    def DOClear(self):
        self.ssidToHit = {}

    def DOWrite(self,hID,hSSID):
        
        if hSSID not in self.ssidToHit:
            self.ssidToHit[hSSID] = []

        self.ssidToHit[hSSID].append(hID)

    # Called after DOwrite
    def isSSIDFired(self,SSID):
        return (SSID in self.ssidToHit)


    def getHits3D(self,trk_xc,trk_yc,trk_phi,trk_theta, trk_z, trk_q, tol=0.01):
        # pseudo Hits are the locations of the hits in the fully phi-symetric geometry
        ps_Hitx, ps_Hity, ps_Hitz, ps_Hitr = self.getPseudoHits3D(trk_xc,trk_yc,trk_phi,trk_theta, trk_z, trk_q, tol)
        
        # check if layer is actually hit
        if (ps_Hitz > self.lengthZ) or (ps_Hitz < -1*self.lengthZ):
            return None,None,None,None,None,None

        ps_HitPhi = np.arctan2(float(ps_Hity),float(ps_Hitx))
        
        modulesHit = []
        
        real_Hits = []

        debug = False

        for m in self.mods:
            if m.isModuleHit3D(ps_HitPhi,ps_Hitz,debug=debug):
                elementID = (self.volume_id,self.layer_id,m.module_id)
                modulesHit.append(elementID)

                hitx, hity, hitz = m.getHits3D(trk_xc,trk_yc,trk_phi,trk_theta, trk_z, trk_q)

                if len(hitx): real_Hits.append((elementID,hitx[0], hity[0], hitz[0]))
        if debug: print(self.layer_id,modulesHit)
                    
        return ps_Hitx, ps_Hity, ps_Hitz, ps_Hitr, modulesHit, real_Hits



    def getPseudoHits3D(self,trk_xc,trk_yc,trk_phi,trk_theta, trk_z,trk_q, tol=0.01,debug=False):
        if debug: print("getPseudoHits3D layer",self.layer_id)
        x_sol, y_sol,z_sol, r_sol = getHitsCylinder3D(self.layerR,trk_xc,trk_yc,trk_phi,trk_theta, trk_z,trk_q, tol,debug)
        return x_sol, y_sol, z_sol, r_sol
