import numpy as np
import matplotlib.pyplot as plt
from TrackHelper import getHitsCylinderXY

class module: 
    
    def __init__(self):
        pass

    def initSimple(self, moduleID, layerR, phiCenter, modLength = 0.14, modTilt = -1*np.pi/12):

        self.module_id = moduleID

        self.layerR  = layerR
        self.phiCenter = phiCenter
        self.modLength = modLength
        self.modTilt = modTilt
        
        self.modCenter = [layerR*np.cos(phiCenter), layerR*np.sin(phiCenter)]
        self.modEdges = self.getGlobalEdges()
        
        # For checking if the module is hit
        self.phiEdges = np.arctan2(self.modEdges[1],self.modEdges[0])

        # 0-2*pi
        self.phiEdges[self.phiEdges < 0] = self.phiEdges[self.phiEdges < 0]+ 2*np.pi

        self.phiEdgeMax = max(self.phiEdges[0],self.phiEdges[1])
        self.phiEdgeMin = min(self.phiEdges[0],self.phiEdges[1])
        if self.phiEdgeMax-self.phiEdgeMin > np.pi:
            self.phiEdgeMin = max(self.phiEdges[0],self.phiEdges[1])
            self.phiEdgeMax = min(self.phiEdges[0],self.phiEdges[1])


        # for getting hit positions
        # check sign
        self.modSlopeXY = (self.modEdges[1][1] - self.modEdges[1][0])/(self.modEdges[0][1] - self.modEdges[0][0])
        self.modOffsetXY = self.modEdges[1][0] - self.modSlopeXY*self.modEdges[0][0]

    def initFromFile(self, moduleData):
        self.moduleData = moduleData

        self.module_id = moduleData["module_id"]
        
        self.trans_localToGlobal = np.array([moduleData["cx"],moduleData["cy"],moduleData["cz"]])
        self.rot_localToGlobal   = np.array([[moduleData["rot_xu"],moduleData["rot_xv"],moduleData["rot_xw"]],
                                             [moduleData["rot_yu"],moduleData["rot_yv"],moduleData["rot_yw"]],
                                             [moduleData["rot_zu"],moduleData["rot_zv"],moduleData["rot_zw"]]
                                            ])
        maxhu = moduleData["module_maxhu"]
        minhu = moduleData["module_minhu"]
        hv    = moduleData["module_hv"]

        self.corners_l = []
        self.corners_l = np.array([[+1*maxhu,+1*hv,0],
                                   [+1*minhu,-1*hv,0],
                                   [-1*minhu,-1*hv,0],
                                   [-1*maxhu,+1*hv,0],
                                   [+1*maxhu,+1*hv,0],
                                  ])
        self.corners_g = (self.rot_localToGlobal.dot(self.corners_l.T).T  + self.trans_localToGlobal)

        self.rs = np.sqrt(self.corners_g[:,0]**2 + self.corners_g[:,1]**2)
        self.r_ave = np.average(self.rs)
        self.r_min = np.min(self.rs)
        self.r_max = np.max(self.rs)

        self.z_max = max(self.corners_g[0:3:2,2])
        self.z_min = min(self.corners_g[0:3:2,2])
        self.z_ave = (self.z_max+self.z_min)*0.5

        self.thetaEdges = [np.arctan2(self.r_min,self.z_ave),np.arctan2(self.r_max,self.z_ave)]
        self.minTheta   = min(self.thetaEdges)
        self.maxTheta   = max(self.thetaEdges)


        self.modEdges = np.array([
                [self.corners_g[1,0],self.corners_g[2,0]],
                [self.corners_g[1,1],self.corners_g[2,1]],
                ])

        # For checking if the module is hit
        self.phiEdges = np.arctan2(self.modEdges[1],self.modEdges[0])
        # 0-2*pi
        self.phiEdges[self.phiEdges < 0] = self.phiEdges[self.phiEdges < 0]+ 2*np.pi

        self.phiEdgeMax = max(self.phiEdges[0],self.phiEdges[1])
        self.phiEdgeMin = min(self.phiEdges[0],self.phiEdges[1])
        if self.phiEdgeMax-self.phiEdgeMin > np.pi:
            self.phiEdgeMin = max(self.phiEdges[0],self.phiEdges[1])
            self.phiEdgeMax = min(self.phiEdges[0],self.phiEdges[1])


        self.modSlopeXY = (self.modEdges[1][1] - self.modEdges[1][0])/(self.modEdges[0][1] - self.modEdges[0][0])
        self.modOffsetXY = self.modEdges[1][0] - self.modSlopeXY*self.modEdges[0][0]

        if (self.corners_g[2,2]-self.corners_g[0,2]) == 0:
            self.modSlopeZR  = None
            self.modOffsetZR = None
        else:
            self.modSlopeZR = (self.rs[2]-self.rs[0])/(self.corners_g[2,2]-self.corners_g[0,2])
            self.modOffsetZR = self.rs[0] - self.modSlopeZR*self.corners_g[0,2]
        

        # for 3D getting the trakc/module crossing
        # point on the sensor plane
        self.p0 = self.corners_g[0]  
        
        # two vectors that span the sensor
        self.v1 = (self.corners_g[0]-self.corners_g[1])
        self.v2 = (self.corners_g[0]-self.corners_g[2])

        # normal to the sensor plane
        self.nhat = np.cross(self.v1,self.v2)
        self.nhat *= 1./np.sqrt(self.nhat.dot(self.nhat))

        # to get dphi in trap modules
        self.dPhidR = (maxhu - minhu)/(2*hv)


    def getGlobalEdges(self):
        # Module in the local frame
        modEdges = np.array([
                [-1*self.modLength, self.modLength],
                [0,0]
                ])
        
        # Local Module Rotation 
        #   Module overlaps 
        tiltM = np.array([
            [np.cos(self.modTilt), -np.sin(self.modTilt)],
            [np.sin(self.modTilt),  np.cos(self.modTilt)]
        ])
        modEdgesTilt = tiltM.dot(modEdges)


        # Global Module Rotation 
        #    Module center is tangent to module layer 
        modRotAngle = self.phiCenter+np.pi/2
        rotM = np.array([
            [np.cos(modRotAngle), -np.sin(modRotAngle)],
            [np.sin(modRotAngle),  np.cos(modRotAngle)]
        ])
        
        # Translation
        translation = np.array(self.modCenter).reshape(2,1)
        
        # Module with local -> global transformation
        modEdgesTransformed = rotM.dot(modEdgesTilt) + translation
        
        return modEdgesTransformed
    
    def drawModuleXY(self,color, detailed=False):
        plt.plot(self.modEdges[0],self.modEdges[1],color,linewidth=3)
        if detailed:
            xLine = np.linspace(-4,4,10)
            yLine = self.modSlopeXY*xLine + self.modOffsetXY
            plt.plot(xLine,yLine,"k--")    

    def drawEndcapModuleXY(self,color="gray"):
        plt.plot(self.corners_g[:,0],self.corners_g[:,1],color=color)


    def drawModuleZR(self,color, detailed=False):

        plt.plot(self.corners_g[0:3:2,2],self.rs[0:3:2],color=color)

        
    def isModuleHitXY(self, ps_HitPhi, tol=1*np.pi/1200, debug=False):
        if debug: print("isModuleHitXY")
        if ps_HitPhi < 0: ps_HitPhi+=2*np.pi
        if debug: print(ps_HitPhi, "vs",self.phiEdges,self.phiEdgeMax,self.phiEdgeMin)
        
        # is lower than upper edge
        dPhiUpperEdge = (ps_HitPhi - self.phiEdgeMax)
        if debug: print("dPhiUpperEdge",dPhiUpperEdge      )
        if dPhiUpperEdge > np.pi: 
            dPhiUpperEdge -= 2*np.pi
            if debug: print("dPhiUpperEdge",dPhiUpperEdge      )
        if dPhiUpperEdge < -np.pi: 
            dPhiUpperEdge += 2*np.pi
            if debug: print("dPhiUpperEdge",dPhiUpperEdge      )


        if debug: print("Pass upper edge req")
        
        # is higher than lower edge
        dPhiLowerEdge = (ps_HitPhi - self.phiEdgeMin)
        if debug: print("dPhiLowerEdge",dPhiLowerEdge      )
        if dPhiLowerEdge > np.pi:
            dPhiLowerEdge -= 2*np.pi
            if debug: print("dPhiLowerEdge",dPhiLowerEdge      )
        if dPhiLowerEdge < -np.pi:
            dPhiLowerEdge += 2*np.pi
            if debug: print("dPhiLowerEdge",dPhiLowerEdge      )

        if debug: print("Diffs",dPhiUpperEdge, dPhiLowerEdge)

        if (dPhiUpperEdge-tol) > 0: 
            return False

        if (dPhiLowerEdge+tol) < 0:
            return False
        if debug: print("Pass lower edge req")
        #if ps_HitPhi < self.phiEdges[0]-tol or ps_HitPhi > self.phiEdges[1]+tol: 
        #    return False
        return True

    def isModuleHit3D(self, ps_HitPhi, ps_Hitz, tol=1*np.pi/1200, debug=False):
        if debug: print("isModuleHit3D")
        if not self.isModuleHitXY(ps_HitPhi, tol,debug):
            return False

        if ps_Hitz < self.z_min or ps_Hitz > self.z_max:
            return False

        return True


    def isDiskModuleHit3D(self,ps_HitPhi, ps_HitTheta, ps_Hitr, tol=1*np.pi/1200, debug=False):
        if debug: print("isDiskModuleHit3D")
        # is theta right 
        if ps_HitTheta < self.minTheta or ps_HitTheta > self.maxTheta:
            if debug: print("\t..fail theta ")
            return False
        if debug: print("\t..pass theta ")

        # is phi right for (rectangle/trapezoid)
        if debug: print("hit phi: ",ps_HitPhi, "vs modphi",self.phiEdgeMax,self.phiEdgeMin)
        # is lower than upper edge
        dPhiUpperEdge = (ps_HitPhi - self.phiEdgeMax)
        if dPhiUpperEdge > np.pi:  dPhiUpperEdge -= 2*np.pi
        if dPhiUpperEdge < -np.pi: dPhiUpperEdge += 2*np.pi
        
        # is higher than lower edge
        dPhiLowerEdge = (ps_HitPhi - self.phiEdgeMin)
        if dPhiLowerEdge > np.pi:  dPhiLowerEdge -= 2*np.pi
        if dPhiLowerEdge < -np.pi: dPhiLowerEdge += 2*np.pi

        #phiCor = self.dPhidR*(ps_Hitr-self.r_min)
        #if debug: print("phiCor:",phiCor,"(",self.dPhidR,ps_Hitr,self.r_min,")")

        if (dPhiUpperEdge-tol) > 0: 
            if debug: print("\t..fail upper phi ")
            return False

        if (dPhiLowerEdge+tol) < 0:
            if debug: print("\t..fail lower phi ")
            return False
        
        if debug: print("\t..pass phi ")
        return True
    
    def getHits(self, trk_xc, trk_yc):
        # Solution of the intersection of a line and a circle
        A = (1 + self.modSlopeXY*self.modSlopeXY)
        bmyc = (self.modOffsetXY - trk_yc)
        B = 2*(self.modSlopeXY*bmyc - trk_xc)
        C = (trk_xc*trk_xc + bmyc*bmyc - (trk_xc*trk_xc + trk_yc*trk_yc))
        coeff = [A,B,C]
        xsols_posible = np.roots(coeff)
        xsols_posible = np.real(xsols_posible)
        ysols_posible = self.modSlopeXY*xsols_posible + self.modOffsetXY
        
        xsols = []
        ysols = []        
        
        # Check which solution is on the module
        hitPhis = np.arctan2(ysols_posible,xsols_posible)
        for xsolItr, xsol_posible in enumerate(xsols_posible):
            if self.isModuleHitXY(hitPhis[xsolItr]):
                xsols.append(xsols_posible[xsolItr])
                ysols.append(ysols_posible[xsolItr])

        return xsols, ysols


    def getHits3D(self, trk_xc, trk_yc, trk_phi, trk_theta, trk_z, trk_q):
        x, y = self.getHits(trk_xc, trk_yc)
        x = np.array(x)
        y = np.array(y)
        r = np.sqrt(x**2+y**2)

        z = r/np.tan(trk_theta) + trk_z
        
        return x, y, z


    def getDiskHits3D(self, trk_xc, trk_yc, trk_phi, trk_theta, trk_z, trk_q):
        trk_tanT = np.tan(trk_theta)

        if self.modSlopeZR is None:
            z_sol = self.z_ave
            r_sol = trk_tanT*(z_sol-trk_z)
            x_sol, y_sol = getHitsCylinderXY(r_sol, trk_xc,trk_yc,trk_phi,trk_q)
            
        else:
            r_sol = (self.modOffsetZR*trk_tanT + trk_z*trk_tanT*self.modSlopeZR)/(self.modSlopeZR - trk_tanT)
            z_sol = (r_sol - self.modOffsetZR)/self.modSlopeZR
            x_sol, y_sol = getHitsCylinderXY(r_sol, trk_xc,trk_yc,trk_phi,trk_q)

        return np.array([x_sol]), np.array([y_sol]), np.array([z_sol])
        
    def isInTower(self, phiCenter, phiWidth):
        if phiCenter < 0: phiCenter += 2*np.pi

        diffEdge1 = (self.phiEdgeMax - phiCenter)
        if diffEdge1 < -1*np.pi: diffEdge1 += 2*np.pi
        if diffEdge1 >  1*np.pi: diffEdge1 -= 2*np.pi
        if abs(diffEdge1) < phiWidth/2: return True

        diffEdge2 = (phiCenter - self.phiEdgeMin)
        if diffEdge2 < -1*np.pi: diffEdge2 += 2*np.pi
        if diffEdge2 >  1*np.pi: diffEdge2 -= 2*np.pi
        if abs(diffEdge2) < phiWidth/2: return True

        return False
