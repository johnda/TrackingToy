import numpy as np
import matplotlib.pyplot as plt
from TrackHelper import getPhiCircle
from TrackHelper import getSSIDS
from layer import layer
from disk  import disk
import pandas as pd

class detectorGeo: 
    
    def __init__(self):
        self.layers = []
        self.disks = []

        # global layer ID
        self.globalLayerID = 0
        self.mapToGlobalID = {}

    def getGlobalLayerID(self,volID, layID):
        return self.mapToGlobalID[(volID,layID)]

    def getLogicalLayerID(self,volID, layID):
        # for now just use globalLayerID mapping
        return self.mapToGlobalID[(volID,layID)]


    def getElementIDFromLogicalLayer(self,logicalLayerID):
        # for now just use globalLayerID mapping
        for elementID in self.mapToGlobalID:
            if logicalLayerID == self.mapToGlobalID[elementID]:
                return elementID
            
        print("ERROR no elementID logicialLayer ",elementID)
        return None

    
    def getLayerOrDisk(self,elementID):
        vol_ID = elementID[0]
        lay_ID = elementID[1]
        for l in self.layers:
            if l.volume_id == vol_ID and l.layer_id == lay_ID:
                return l

        for d in self.disks:
            if d.volume_id == vol_ID and d.layer_id == lay_ID:
                return d


        print("ERROR no detector element for ",elementID)
        return None

    def initSimple(self,rDet,nModPerLayer):
        for layerID, layerR  in enumerate(rDet):
            self.layers.append(layer())
            self.layers[-1].initSimple(layerID,rDet[layerID],nModPerLayer[layerID])

            self.mapToGlobalID[(self.layers[-1].volume_id,self.layers[-1].layer_id)] = self.globalLayerID
            self.globalLayerID += 1

    def initFromFile(self,layerData,nModulesInPhi,endcapData=None,nModInR=None):
        for layItr, ld in enumerate(layerData):
            self.layers.append(layer())
            self.layers[-1].initFromFile(ld,nModulesInPhi[layItr])
            
            self.mapToGlobalID[(self.layers[-1].volume_id,self.layers[-1].layer_id)] = self.globalLayerID
            self.globalLayerID += 1

        if not endcapData is None:
            for diskItr, dd in enumerate(endcapData):
                self.disks.append(disk())
                self.disks[-1].initFromFile(dd,nModInR[diskItr])

                self.mapToGlobalID[(self.disks[-1].volume_id,self.disks[-1].layer_id)] = self.globalLayerID
                self.globalLayerID += 1

    def drawDetector(self,ax,detailed=False):
        for l in self.layers: 
            l.drawLayer(ax, detailed)

    def drawDetectorXY(self,ax,detailed=False):
        for l in self.layers: 
            l.drawLayerXY(ax, detailed)

    def drawDetectorZR(self,ax,detailed=False):
        for l in self.layers:
            l.drawLayerZR(ax,detailed)

        for d in self.disks:
            d.drawLayerZR(ax,detailed)
            
    def drawDetectorModules(self,modulesToDraw, detailed=False):
        for elementID in modulesToDraw:
            thisLayDisk = self.getLayerOrDisk(elementID)
            thisLayDisk.drawDetectorModules(elementID[2], detailed)

    def drawDetectorModulesZR(self,modulesToDraw, detailed=False):
        for elementID in modulesToDraw:
            thisLayDisk = self.getLayerOrDisk(elementID)
            thisLayDisk.drawDetectorModulesZR(elementID[2], detailed)

    def drawDiskModulesXY(self,modulesToDraw, detailed=False):
        for elementID in modulesToDraw:
            thisLayDisk = self.getLayerOrDisk(elementID)
            thisLayDisk.drawDiskModulesXY(elementID[2], detailed)

            
    def getHits(self,trk_rc,trk_phi,trk_q, tol=0.01):
        if trk_phi<0: trk_phi += 2*np.pi
        trk_phi_circle = getPhiCircle(trk_phi,trk_q)
        trk_xc = trk_rc*np.cos(trk_phi_circle)
        trk_yc = trk_rc*np.sin(trk_phi_circle)

        # Global positions of the pseudo-hits 
        #   pseudo Hits are the locations of the hits in the fully phi-symetric geometry

        xs = []
        ys = []
        
        # List of wihch modules are hit
        #   list of phiIDs for each layer
        modulesHit = []
        
        # actual detector hits.
        #   Format:   layerID  phiID  list-of-gobalX-positions  list-of-gobalY-positions
        actualHits = []
        
        for l in self.layers:
            x, y, mods, hits = l.getHits(trk_xc,trk_yc, trk_phi, trk_q)
            
            xs.append(x)
            ys.append(y)
            modulesHit += mods
            actualHits += hits

        return xs, ys, modulesHit, actualHits

    
    
    def makeTower(self, phiCenter, phiWidth, nSS):
        self.towerPhiCenter = phiCenter
        self.towerPhiWidth = phiWidth
        for layItr, l in enumerate(self.layers): 
            l.makeTower(phiCenter, phiWidth, nSS[layItr])


    def drawTower(self, ax, detailed=False,drawPSLayer=True):
        # Drew the wedge
        r = self.layers[-1].layerR
        phi1 = self.towerPhiCenter + self.towerPhiWidth/2
        phi2 = self.towerPhiCenter - self.towerPhiWidth/2
        plt.plot([np.cos(phi2)*r,0,np.cos(phi1)*r],[np.sin(phi2)*r,0,np.sin(phi1)*r],'--')

        # Draw the layers
        for l in self.layers: l.drawTower(ax, detailed,drawPSLayer)
        


    # Takes a list of hits and checks if they are all contained in the tower
    def hitsAllInTower(self,hitList, debug=False):
        for h in hitList:
            elementID = h[0]
            hX        = h[1]
            hY        = h[2]
            thisLayDisk = self.getLayerOrDisk(elementID)
            if not thisLayDisk.hitInTower(hX,hY,debug):
                if debug: print("failed layer",elementID)
                return False

        return True


    # Takes list of hits with format (hitID,hitX,hitY,hitZ,volID,layID)
    # Returns a list with format     (hitID,hitX,hitY,hitZ,volID,layID,SSID, logicalLayID)
    def addSSIDs(self,hitList,filterOnTower=False,debug=False):

        # One list per layer
        hitsAndSSIDS = []

        for h in hitList:
            volID  = h[4]
            layID  = h[5]
            hX     = h[1]
            hY     = h[2]
            thisLayer = self.getLayerOrDisk((volID,layID,None))

            if filterOnTower and not thisLayer.hitInTower(hX,hY):
                continue
            if debug:
                print("adding",[thisLayer.getSSID(hX,hY)],"to",h)
                print("\tgives",list(h) +  [self.getLogicalLayerID(volID,layID), thisLayer.getSSID(hX,hY)] )
            hitsAndSSIDS.append( list(h) +  [self.getLogicalLayerID(volID,layID), thisLayer.getSSID(hX,hY)] )

        return hitsAndSSIDS


    # Takes a list of SSIDs  of the form (layer, SSIDS)  to be drawn
    def drawSSIDs(self,ax,SSIDList):
        for ssid in SSIDList: 
            logicalLayer = ssid[0]
            volID, layID = self.getElementIDFromLogicalLayer(logicalLayer)
            thisLayer = self.getLayerOrDisk((volID,layID))
            thisLayer.drawSSID(ax,ssid[1])



    def DOWrite(self,hitsAndSSIDs):

        for l in self.layers:
            l.DOClear()

        for h in hitsAndSSIDs:
            hvolID = h[4]
            hlayID = h[5]
            hID    = h[0]
            hSSID  = h[8]
            thisLayer = self.getLayerOrDisk((hvolID,hlayID))
            thisLayer.DOWrite(hID,hSSID)



    def isPatternFired(self,p,debug=False):

        # Check each layer 
        for layItr, l in enumerate(self.layers): 
            #before p_ssid = int((p / 100**layItr)) % 100
            p_ssid = int((p % 100**(layItr+1) / 100**layItr))
            #p_ssid = int((p % 100**(layItr+1))) % 100

            if not l.isSSIDFired(p_ssid):
                if debug: print(layItr,"Failed")
                if debug: print(l.ssidToHit)
                if debug: print(p_ssid)
                if debug: print(p_ssid in l.ssidToHit)
                return False
            if debug: print(layItr,"Passed")
        return True



    def getSolutions(self, matchedPatterns, trkIDInit=0):

        evt_id = []
        hit_id = []
        trk_id = []

        trkID = trkIDInit

        for mp in matchedPatterns:
            trkID +=1 
            thisSSIDs = getSSIDS(mp,nLay=len(self.layers))

            this_evt_id = []
            this_hit_id = []
            this_trk_id = []
            for laySSID in thisSSIDs:
                volID, layID = self.getElementIDFromLogicalLayer(laySSID[0])
                thisLayDisk = self.getLayerOrDisk((volID,layID))
                for hit in thisLayDisk.ssidToHit[laySSID[1]]:
                    # Sub optimial !
                    if hit not in hit_id:
                        this_evt_id.append(0)
                        this_hit_id.append(hit)
                        this_trk_id.append(trkID)

            if len(this_hit_id) > 3:
                evt_id += this_evt_id
                hit_id += this_hit_id
                trk_id += this_trk_id
                    
    
        solnDict = {"event_id" : pd.Series(evt_id),
                    "hit_id" : pd.Series(hit_id),
                    "track_id" : pd.Series(trk_id),
                    }

        solns = pd.DataFrame(solnDict)
        return solns, (evt_id, hit_id, trk_id)


    def getHits3D(self,trk_rc,trk_phi,trk_theta,trk_z0,trk_q, tol=0.01):
        if trk_phi<0: trk_phi += 2*np.pi
        trk_phi_circle = getPhiCircle(trk_phi,trk_q)
        trk_xc = trk_rc*np.cos(trk_phi_circle)
        trk_yc = trk_rc*np.sin(trk_phi_circle)

        # Global positions of the pseudo-hits 
        #   pseudo Hits are the locations of the hits in the fully phi-symetric geometry
        xs = []
        ys = []
        zs = []
        rs = []
        
        # List of wihch modules are hit
        #   list of phiIDs for each layer
        modulesHit = []
        
        # actual detector hits.
        #   Format:   layerID  phiID  list-of-gobalX-positions  list-of-gobalY-positions
        actualHits = []
        
        for l in self.layers:
            x, y, z, r, mods, hits = l.getHits3D(trk_xc,trk_yc, trk_phi, trk_theta, trk_z0, trk_q)
            if not x is None:
                xs.append(x)
                ys.append(y)
                zs.append(z)
                rs.append(r)
                modulesHit += mods
                actualHits += hits

        for d in self.disks:
            x, y, z, r, mods, hits  = d.getHits3D(trk_xc,trk_yc, trk_phi, trk_theta, trk_z0, trk_q)
            if not x is None:
                xs = np.append(xs,x)
                ys = np.append(ys,y)
                zs = np.append(zs,z)            
                rs = np.append(rs,r)                        
                modulesHit += mods
                actualHits += hits

        return xs, ys, zs, rs, modulesHit, actualHits
