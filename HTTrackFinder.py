import numpy as np
import pandas as pd
from TrackHelper import getPhiCircle
import scipy.optimize as opt
import matplotlib.pyplot as plt



def correctTrackPhis(fullTrackInfo,debug=False):
    firstPhi = fullTrackInfo[1][0,-1]
    for thisHitInfo in fullTrackInfo[1]:
        phiDiff = thisHitInfo[-1] - firstPhi
        if debug: print("phiDiff",phiDiff)
        if phiDiff < -np.pi: thisHitInfo[-1] += 2*np.pi
        if phiDiff >  np.pi: thisHitInfo[-1] -= 2*np.pi
        if debug: 
            phiDiff = thisHitInfo[-1] - firstPhi
            print("phiDiff now",phiDiff)            
        

def DORead(trackInfo, hitInfo):
    fullTrackInfo = []
    fullTrackInfo.append(trackInfo[0])
    fullTrackInfo.append([])
    for hitIDsThisLayer in trackInfo[1]:
        for thisHitID in hitIDsThisLayer:
            fullTrackInfo[-1].append(list(hitInfo[hitInfo[:,0] == thisHitID][0]))
    fullTrackInfo[-1] = np.array(fullTrackInfo[-1])
    return fullTrackInfo

def drawTrackToFit(fullTrackInfo,color="k",hitRIndex=-2, hitPhiIndex=-1):
    plt.plot(fullTrackInfo[1][:,hitRIndex],fullTrackInfo[1][:,hitPhiIndex],color+"o")

def fitFunPhiR(p, x):
    return np.arccos(x*p[1]/2)+p[0]-np.pi/2    

def errFunPhiR(p, x, y):
    return fitFunPhiR(p, x) - y 

def chi2FunPhiR(p, x, y, err=0.0005):
    return ((fitFunPhiR(p, x) - y)/err)**2

def drawTrackFit(trkInfo,color="k",hitRIndex=-3):
    plt.plot(trkInfo[1][:,hitRIndex],fitFunPhiR(trkInfo[0][0:3], trkInfo[1][:,hitRIndex]),color+"--")


def fitTrackXY(rawTrack, chi2Cut = 0.01, hitRIndex=-2, hitPhiIndex=-1): 
    # Start with initial track params from the road
    p0 = rawTrack[0][0:3]

    # Do the fit
    p1,success = opt.leastsq(chi2FunPhiR, p0[:], args=(rawTrack[1][:,hitRIndex], rawTrack[1][:,hitPhiIndex]))

    # assing the new track parammert
    newTrack = [list(rawTrack[0])]
    newTrack[0][0] = p1[0]
    newTrack[0][1] = p1[1]
    newTrack[0][2] = p1[2]

    # correct phi
    if newTrack[0][0] > 2*np.pi: newTrack[0][0] = newTrack[0][0] - 2*np.pi
    if newTrack[0][0] < 0: newTrack[0][0] = newTrack[0][0] + 2*np.pi
    
    newTrack.append(rawTrack[1][chi2FunPhiR(p1, rawTrack[1][:,hitRIndex], rawTrack[1][:,hitPhiIndex])  < chi2Cut])

    X2withOutliers = np.sum(chi2FunPhiR(p1, rawTrack[1][:,hitRIndex], rawTrack[1][:,hitPhiIndex]))
    newTrack.append(X2withOutliers)
    X2noOutliers   = np.sum(chi2FunPhiR(p1, newTrack[1][:,hitRIndex], newTrack[1][:,hitPhiIndex]))
    newTrack.append(X2noOutliers)
    
    X2PerHit = abs(chi2FunPhiR(p1, newTrack[1][:,hitRIndex], newTrack[1][:,hitPhiIndex]))
    newTrack[1] = np.c_[newTrack[1],X2PerHit]

    return newTrack

def drawTrackToFitZR(fullTrackInfo,color="k",hitRIndex=-2):
    plt.plot(fullTrackInfo[1][:,3],fullTrackInfo[1][:,hitRIndex],color+"o")

def fitFunZR(p, x):
    return p[1]*(x-p[0])

def errFunZR(p, x, y):
    return fitFunZR(p, x) - y

def chi2FunZR(p, x, y, err=1):
    return ((fitFunZR(p, x) - y)/err)**2

def drawTrackFitZR(trkInfo,color="k",hitZIndex=3):
    plt.plot(trkInfo[1][:,hitZIndex],fitFunZR([trkInfo[0][3], np.tan(trkInfo[0][4])],trkInfo[1][:,hitZIndex]), color+"--")

def fitTrackRZ(rawTrack, chi2Cut = 0.01, hitZIndex=3, hitRIndex=5):

    # Start with initial track params from the road 
    p0 = [rawTrack[0][3], np.tan(rawTrack[0][4])]

    # Do the fit
    p1,success = opt.leastsq(chi2FunZR, p0[:], args=(rawTrack[1][:,hitZIndex], rawTrack[1][:,hitRIndex]))

    if success > 4:
        p0 = [rawTrack[0][3], -np.tan(rawTrack[0][4])]
        p1,success = opt.leastsq(chi2FunZR, p0[:], args=(rawTrack[1][:,hitZIndex], rawTrack[1][:,hitRIndex]))
    
    # assing the new track parammert
    newTrack = [list(rawTrack[0])]
    newTrack[0][3] = p1[0]
    newTrack[0][4] = np.arctan(p1[1])
    if newTrack[0][4] < 0: newTrack[0][4] = np.pi+newTrack[0][4]

    # chi2 from XY-fit
    chi2XY  = rawTrack[1][:,-1]
    # chi2 from ZR-fit
    chi2RZ  = chi2FunZR(p1, rawTrack[1][:,hitZIndex], rawTrack[1][:,hitRIndex])
    # Add in quad
    chi2Tot = np.sqrt(chi2XY*chi2XY + chi2RZ*chi2RZ)

    # Make the chi2 cut
    newTrack.append(rawTrack[1][chi2Tot < chi2Cut])

    X2withOutliers = np.sum(chi2Tot)
    newTrack.append(X2withOutliers)
    X2noOutliers   = np.sum(chi2Tot[chi2Tot < chi2Cut])
    newTrack.append(X2noOutliers)

    X2PerHit = chi2Tot[chi2Tot < chi2Cut]
    newTrack[1] = np.c_[newTrack[1],X2PerHit]
    
    return newTrack

def do3DFit(rawTrack, hitRIndex, hitPhiIndex, hitZIndex):
    # Start with initial track params from the road
    p0_RPhi = rawTrack[0][0:3]
    p0_ZR   = [rawTrack[0][3], np.tan(rawTrack[0][4])]
    
    # Do the fits
    p1_RPhi,success_RPhi = opt.leastsq(chi2FunPhiR, p0_RPhi[:], 
                                       args=(rawTrack[1][:,hitRIndex], rawTrack[1][:,hitPhiIndex]))
    p1_ZR,  success_ZR   = opt.leastsq(chi2FunZR, p0_ZR[:], 
                                       args=(rawTrack[1][:,hitZIndex], rawTrack[1][:,hitRIndex]))
    if success_ZR > 4:
        p0_ZR = [rawTrack[0][3], -np.tan(rawTrack[0][4])]
        p1_ZR,success_ZR = opt.leastsq(chi2FunZR, p0_ZR[:], 
                                       args=(rawTrack[1][:,hitZIndex], rawTrack[1][:,hitRIndex]))
    
    #
    # assign the new track parameters
    #
    newTrack = [list(rawTrack[0])]
    newTrack[0][0] = p1_RPhi[0]
    # correct phi
    if newTrack[0][0] > 2*np.pi: newTrack[0][0] = newTrack[0][0] - 2*np.pi
    if newTrack[0][0] < 0: newTrack[0][0] = newTrack[0][0] + 2*np.pi
    
    newTrack[0][1] = p1_RPhi[1]
    newTrack[0][2] = p1_RPhi[2]
    
    # assing the new track parammert
    newTrack[0][3] = p1_ZR[0]
    newTrack[0][4] = np.arctan(p1_ZR[1])
    if newTrack[0][4] < 0: newTrack[0][4] = np.pi+newTrack[0][4]
        
        
    # 
    #  Add the hits
    #
    newTrack.append(rawTrack[1])
    
    #
    # Calc chi2
    #
    chi2PerHit_RPhi = chi2FunPhiR(p1_RPhi, rawTrack[1][:,hitRIndex], rawTrack[1][:,hitPhiIndex])
    chi2PerHit_ZR   = chi2FunZR  (p1_ZR,   rawTrack[1][:,hitZIndex], rawTrack[1][:,hitRIndex])
    chi2PerHit      = np.sqrt(chi2PerHit_RPhi*chi2PerHit_RPhi + chi2PerHit_ZR*chi2PerHit_ZR)
    chi2Total       = np.sum(chi2PerHit)
    
    #
    # Add the chi2
    #
    newTrack.append(chi2Total)
    newTrack[1] = np.c_[newTrack[1],chi2PerHit]
    
    return newTrack

def iterativeFit(rawTrack, chi2Cut, hitRIndex, hitPhiIndex, hitZIndex,debug=False):

    # Always do one fit
    newTrack = do3DFit(rawTrack, hitRIndex, hitPhiIndex, hitZIndex)
    
    # this this chi2
    currentChi2 = newTrack[2]
    if debug: 
        print("chi2:",currentChi2)
        print("nHits:",len(newTrack[1]))

    while(currentChi2 > chi2Cut):

        # remove the hit with the largest chi2
        maxchi2Hit = np.argmax(newTrack[1][:,-1])
        newTrack[1] = np.delete(newTrack[1],maxchi2Hit,0)
            
        # If fail nhits kill track
        nHits       = len(newTrack[1])
        if(nHits < 8):
            return []
        
        # do fit 
        newTrack = do3DFit(newTrack, hitRIndex, hitPhiIndex, hitZIndex)
        
        # get chi2 
        currentChi2 = newTrack[2]
        if debug: 
            print("chi2:",currentChi2)
            print("nHits:",nHits)

        
    return newTrack

# Logic to remove duplicate tracks (not needed yet)
#roadsToRemove = []
#for rFirstIndx in range(len(allRecoRoadsTest)):
#    if rFirstIndx in roadsToRemove: continue
#    thisRoad = allRecoRoadsTest[rFirstIndx]
#    for rOtherIndx in range(rFirstIndx,len(allRecoRoadsTest)):
#        if rOtherIndx == rFirstIndx: continue
#        if thisRoad == allRecoRoadsTest[rOtherIndx]: 
#            print("kill",rOtherIndx,"b/c of",rFirstIndx)
#            roadsToRemove.append(rOtherIndx)
#roadsToRemove.sort(reverse=True)
#print(roadsToRemove)   
#for dup in roadsToRemove:
#    allRecoRoadsTest.pop(dup)
#

class HTHitDB:

    def __init__(self,nLayers, xBins, yBins):
        self.nLayers   = nLayers
        self.xBins     = xBins
        self.nXBins  = len(xBins)

        self.yBins = yBins
        self.nYBins = len(yBins)


        #
        # seperatate map per layer
        #
        self.counts     = []
        self.hits_idx   = []
        for iLay in range(self.nLayers):
            self.counts  .append([])
            self.hits_idx.append([])

            # each hitCount layer will be 2D nXBins x nCurveBins
            for iX in range(self.nXBins-1):
                self.counts  [-1].append([])
                self.hits_idx[-1].append([])

                for iY in range(self.nYBins-1):
                    self.counts  [-1][-1].append(0)
                    self.hits_idx[-1][-1].append([])

        self.counts_Full = []

        # each hitCount layer will be 2D nXBins x nCurveBins
        for iX in range(self.nXBins-1):
            self.counts_Full.append([])
            for iY in range(self.nYBins-1):
                self.counts_Full[-1].append(0)

        #
        #  for each layer, map of hit index to the list of iX icurv indices)
        #
        self.hitBank = []
        self.layerActive = [] # flag turned on if hits in a given layer
        for i in range(self.nLayers):        
            self.hitBank.append({})
            self.layerActive.append(False)

    def clear(self):

        #
        # seperatate map per layer
        #
        for iLay in range(self.nLayers):
            if not self.layerActive[iLay]: continue

            # each hitCount layer will be 2D nXBins x nCurveBins
            for iX in range(self.nXBins-1):

                for iY in range(self.nYBins-1):
                    self.counts  [iLay][iX][iY] = 0
                    self.hits_idx[iLay][iX][iY] = []


        # each hitCount layer will be 2D nXBins x nCurveBins
        for iX in range(self.nXBins-1):
            for iY in range(self.nYBins-1):
                self.counts_Full[iX][iY] = 0

        for i in range(self.nLayers):        
            self.layerActive[i] = False
            self.hitBank[i] = {}



    def newHit(self,layer,hitIndex):
        if not self.layerActive[layer]: 
            self.layerActive[layer] = True
        self.hitBank[layer][hitIndex] = []

    def addHit(self,layer,hitIndex,iX,iY):
        self.hitBank[layer][hitIndex].append((iX,iY))

    def removeHit(self,layer,hitIndx):
        
        for hitBin in self.hitBank[layer][hitIndx]:
            iX = hitBin[0]
            iY = hitBin[1]
            
            # decrement 
            self.counts[layer][iX][iY] -= 1 

            # propograte to full counter
            if self.counts[layer][iX][iY] == 0:
                self.counts_Full[iX][iY] -= 1

            self.hits_idx[layer][iX][iY].remove(hitIndx)
        
        self.hitBank[layer].pop(hitIndx)

        #
        # ReMake the arrays
        #
        self.combinedHitsCounts = np.array(self.counts_Full)

    def getNHits(self):
        nHits = 0
        for layBank in self.hitBank:
            nHits += len(layBank)
        return nHits

    def combineLayerMaps(self):

        #
        #  Turn the hits into layer laps
        #
        for iLay in range(self.nLayers):

            if not self.layerActive[iLay]: continue

            for iHit in self.hitBank[iLay]:
                for hitBin in self.hitBank[iLay][iHit]:
                    iX = hitBin[0]
                    iY = hitBin[1]
                    self.counts[iLay][iX][iY] += 1 
                    self.hits_idx[iLay][iX][iY].append(iHit)
            
            
        #
        #  Combine the layer maps 
        #
        for iLay in range(self.nLayers):
            if not self.layerActive[iLay]: continue
            for iX in range(self.nXBins-1):
                for iY in range(self.nYBins-1):
                    if self.counts[iLay][iX][iY] > 0:  self.counts_Full[iX][iY] += 1

        #
        # Make the arrays
        #
        self.combinedHitsCounts = np.array(self.counts_Full)

    def getMax(self):
        max_idx = np.unravel_index(np.argmax(self.combinedHitsCounts, axis=None), self.combinedHitsCounts.shape)
        return self.combinedHitsCounts[max_idx], max_idx

    def getXCenter(self,iX):
        xMin    = self.xBins[iX]
        xMax    = self.xBins[iX+1]
        return 0.5*(xMin+xMax)
        
    def getYCenter(self,iY):
        yMin    = self.yBins[iY]
        yMax    = self.yBins[iY+1]
        return 0.5*(yMin+yMax)

    def findTrack(self,overlapLogicFirstHit=True,debug=False):
        max, max_idx = self.getMax()
        
        iX  = max_idx[0]
        iY = max_idx[1]

        xCenter = self.getXCenter(iX)
        yCenter = self.getYCenter(iY)

        thisTrack = ([xCenter,yCenter,np.sign(yCenter)], [])
        
        for iLay in range(self.nLayers):
            
            # Always take the "first" hit for now. Need better logic to pick the right one
            if overlapLogicFirstHit:
                if len(self.hits_idx[iLay][iX][iY]):
                    if debug: print("\t\tPossible Hits",self.hits_idx[iLay][iX][iY] )
                    thisTrack[1].append( [ self.hits_idx[iLay][iX][iY][0] ] )
                else:
                    thisTrack[1].append( [  ] )
            
            else:
                # Take All hits, better score / violation of kaggle rules
                hitsInThisLayer = []
                for h in self.hits_idx[iLay][iX][iY]:            
                    hitsInThisLayer.append(h)
            
                thisTrack[1].append( hitsInThisLayer )

        return thisTrack
            

class HTTrackFinding: 
    
    def __init__(self, xMin, xMax, nXBins, yMin, yMax, nYBins,getHTFunction,nLayers=6,minHitsPerTrack=5):
        self.xMin    = xMin  
        self.xMax    = xMax  
        self.nXBins  = nXBins 
        self.yMin    = yMin 
        self.yMax    = yMax 
        self.nYBins  = nYBins
        self.nLayers = nLayers
        self.minHitsPerTrack = minHitsPerTrack
        self.getHT   = getHTFunction

        self.xBins = np.linspace(xMin,  xMax,  nXBins )
        self.yBins = np.linspace(yMin, yMax, nYBins)
        self.HitDB = HTHitDB( self.nLayers, self.xBins, self.yBins)


    def clear(self):
        self.HitDB.clear()
        self.tracks = []

    #def getHT(self,hitR,hitPhi,phi):
    #    # Convert 0-2Pi
    #    if hitPhi<0: hitPhi += 2*np.pi
    #    phi_circle = getPhiCircle(phi,1)
    #    return 2/hitR*(np.cos(hitPhi-phi_circle))



    def fillHT(self,layer,hitIndex,hitX,hitY,iX,xMin,xMax,debug=False):
        debug = False
        if debug: print("In fillHT",hitX,hitY,"(",xMin,xMax,")")
        y_at_x_min = self.getHT(hitX,hitY,xMin)
        y_at_x_max = self.getHT(hitX,hitY,xMax)
        if debug: print("y_at_x_min",y_at_x_min)
        if debug: print("y_at_x_max",y_at_x_max)

        if y_at_x_min < self.yMin and y_at_x_max < self.yMin: return
        if y_at_x_min > self.yMax and y_at_x_max > self.yMax: return

        # get the max and min curvatures consistent with this hit nad phi
        y_min = min(y_at_x_min, y_at_x_max)
        y_max = max(y_at_x_min, y_at_x_max)

        if debug: print("filling HitMap")
        # Now fill in the hitMap
        for iY in range(self.nYBins-1): 
            yMin    = self.yBins[iY]
            yMax    = self.yBins[iY+1]
            yCenter = 0.5*(yMin+yMax)

            #if yCenter > y_min and yCenter < y_max:
            if (y_min > yMin and y_min < yMax) or  (y_max > yMin and y_max < yMax) or (y_min < yMin and y_max > yMax):  
                if debug: print("Adding Hit")
                self.HitDB.addHit(layer,hitIndex,iX,iY)

    
    def addHit(self,layer,hitIndex,hitX,hitY,doRangeCut=True):
        
        self.HitDB.newHit(layer,hitIndex)

        for iX in range(self.nXBins-1): 
            xMin    = self.xBins[iX]
            xMax    = self.xBins[iX+1]
            xCenter = 0.5*(xMin+xMax)

            # needed for pt / phi
            if doRangeCut:
                # Filter phi
                # only look in direction of a valid track
                phiDiff     = abs(xCenter - hitY)
                
                #  Logic to handle wrap around
                if phiDiff > np.pi: phiDiff = 2*np.pi-phiDiff
                if phiDiff > np.pi/2: continue
            
            #
            # tracks
            # 
            self.fillHT(layer,hitIndex,hitX,hitY,iX,xMin,xMax)

        
    def combineLayerMaps(self):
        
        self.HitDB.combineLayerMaps()
                    
            
    def removeHitsFromTrack(self,thisTrack,removeOverlap=True):
        for iLay, hitListInLayer in enumerate(thisTrack[1]):
            for hitIndx in hitListInLayer:
                # Sub Optimal
                if len(hitListInLayer) == 1 or removeOverlap:
                    self.HitDB.removeHit(iLay,hitIndx)

    def removeHitsFromFitTrack(self,thisTrack):
        hitIDs   = thisTrack[1][0]
        layerIDs = thisTrack[-2]
        for iItr, hitID in enumerate(hitIDs):        
            if hitID in self.HitDB.hitBank[int(layerIDs[iItr])]:
                self.HitDB.removeHit(int(layerIDs[iItr]),int(hitID))

    def findTracks(self,overlapLogicFirstHit=True,debug=False):
        if debug: print("Entering findTracks")
        # output Tracks
        self.tracks = []

        max_hit, max_hit_idx = self.HitDB.getMax()
        haveTracks = (max_hit >= (self.minHitsPerTrack))

        # Logic to remove overlap if stuck
        hitsLeft = self.HitDB.getNHits()
        removeOverlap = False
        
        if debug:
            print("max_hit",max_hit)
            print("haveTracks",haveTracks)
            print("hitsLeft",hitsLeft)
            print("removeOverlap",removeOverlap)

        while haveTracks:

            self.tracks.append(self.HitDB.findTrack(overlapLogicFirstHit,debug))
            if debug: print("trackFound",self.tracks[-1])

            #
            # Remove found hits
            #
            self.removeHitsFromTrack(self.tracks[-1],removeOverlap)
            
            #
            # Calculate Have track 
            # 
            max_hit, max_hit_idx = self.HitDB.getMax()
            haveTracks = (max_hit >= (self.minHitsPerTrack))#4 )#(

            if hitsLeft == self.HitDB.getNHits():
                removeOverlap = True
            else:
                removeOverlap = False
                hitsLeft = self.HitDB.getNHits()


            if debug:
                print("max_hit",max_hit)
                print("haveTracks",haveTracks)
                print("hitsLeft",hitsLeft)
                print("removeOverlap",removeOverlap)


        if debug: print("Leaving findTracks")

        return self.tracks


    def getSolutionsFromTracks(self,inTracks):
        evt_id = []
        hit_id = []
        trk_id = []
        for trkItr, thisTrack in enumerate(inTracks):
            for thisHit in thisTrack[1]:
                for h in thisHit: 
                    evt_id.append(0)
                    hit_id.append(h)
                    trk_id.append(trkItr)

        solnDict = {"event_id" : pd.Series(evt_id),
                    "hit_id" : pd.Series(hit_id),
                    "track_id" : pd.Series(trk_id),
                    }

        soln = pd.DataFrame(solnDict)
        return soln
        

    def getSolutions(self):
        return self.getSolutionsFromTracks(self.tracks)

    def getHitsInBin(self,iX,iY,hitInfo):
        matchedHits = []

        #print(len(self.HitDB.hits_idx[0][0]))
        #print(len(self.HitDB.hits_idx[0][0][0]))

        for layItr in range(self.nLayers):
            for hitID in self.HitDB.hits_idx[layItr][iX][iY]:
                matchedHits.append(hitInfo[hitInfo[:,0] == hitID][0])
        matchedHits = np.array(matchedHits)
        return matchedHits

    def getHitsInBinDF(self,iX,iY,hitInfo):
        hits_IDs = hitInfo["hit_id"]
        
        hit_id_Match = []
        
        for hID in hits_IDs:
            
            isMatch = False
            for layItr in range(self.nLayers):

                for hitID in self.HitDB.hits_idx[layItr][iX][iY]:
                    if hitID == hID: 
                        isMatch = True
                        break

                if isMatch: break
                
            hit_id_Match.append(isMatch)


        return hitInfo[hit_id_Match]


    def fitTracks(self, inTracks, hitInfo, chi2Cut=0.01, hitRIndex=-2, hitPhiIndex=-1): 
        fittedTracks = [] 
        
        for thisTrack in inTracks:
            fullTrackInfo = DORead(thisTrack,hitInfo)
            
            correctTrackPhis(fullTrackInfo)
            newTrack  = fitTrackXY(fullTrackInfo,chi2Cut, hitRIndex, hitPhiIndex)

            nHitsNewTrack = len(newTrack[1])
            if nHitsNewTrack < self.minHitsPerTrack: 
                continue

            newTrack.append(list(newTrack[1][:,-1]))
            newTrack[1] = [list(newTrack[1][:,0])]

            fittedTracks.append(newTrack)

        return fittedTracks

    def fitTracks3D(self, inTracks, hitInfo, chi2Cut=0.01, hitRIndex=5, hitPhiIndex=-1, hitZIndex=3): 
        fittedTracks = [] 
        
        for thisTrack in inTracks:
            fullTrackInfo = DORead(thisTrack,hitInfo)
            
            correctTrackPhis(fullTrackInfo)
            # Only make chi2 cut on the combined 3D chi2
            newTrack2D  = fitTrackXY(fullTrackInfo,chi2Cut=1e6,     hitRIndex=hitRIndex, hitPhiIndex=hitPhiIndex)
            newTrack    = fitTrackRZ(newTrack2D,   chi2Cut=chi2Cut, hitZIndex=hitZIndex, hitRIndex=hitRIndex)

            nHitsNewTrack = len(newTrack[1])
            if nHitsNewTrack < self.minHitsPerTrack: 
                continue

            newTrack.append(list(newTrack[1][:,-1]))
            newTrack[1] = [list(newTrack[1][:,0])]

            fittedTracks.append(newTrack)

        return fittedTracks

    def fitTracks3DIter(self, inRoads, hitInfo, chi2Cut=1, hitRIndex=5, hitPhiIndex=-1, hitZIndex=3, addRoadIndx=False): 
        fittedTracks = [] 
        
        for roadIndx, thisRoad in enumerate(inRoads):
            fullRoadInfo = DORead(thisRoad,hitInfo)
            
            correctTrackPhis(fullRoadInfo)

            fitTrack = iterativeFit(fullRoadInfo, chi2Cut, hitRIndex, hitPhiIndex, hitZIndex)
            if not len(fitTrack): continue

            # Add the road index
            if addRoadIndx:
                fitTrack.append(roadIndx)

            # Add the GID of the its on the end
            fitTrack.append(list(fitTrack[1][:,(hitRIndex-1)]))

            # Add the chi2 on the end
            fitTrack.append(list(fitTrack[1][:,-1]))
            fitTrack[1] = [list(fitTrack[1][:,0])]

            fittedTracks.append(fitTrack)

        return fittedTracks



    def removeOverlapNHits(self,fittedTracks, hitIdsToClean):
        for hitID in hitIdsToClean:
    
            # get max nHits for this hitID
            maxNHits = 0
            
            trackIDs = [] # so we dont have to do the full loop again or calculate X2
            for tIndex, trackInfo in enumerate(fittedTracks):
                thisTrackHitIDs = trackInfo[1][0]
                for hIndex, thisHitID in enumerate(thisTrackHitIDs):
                    if hitID == thisHitID:
                        thisNHits = len(trackInfo[-1])
                        if thisNHits > maxNHits:
                            maxNHits = thisNHits
                        trackIDs.append((tIndex,thisNHits,hIndex))
                        
            # remove hit from other tracsk
            for tIndex in trackIDs:
                if tIndex[1] < maxNHits:
                    fittedTracks[int(tIndex[0])][1][0].pop(tIndex[2])
                    fittedTracks[int(tIndex[0])][-1]  .pop(tIndex[2])



    def removeOverlapX2(self,fittedTracks, hitIdsToClean):
        for hitID in hitIdsToClean:
    
            # get lowest chi2 for this hitID
            minX2 = 1e6
            
            trackIDs = [] # so we dont have to do the full loop again or calculate X2
            for tIndex, trackInfo in enumerate(fittedTracks):
                thisTrackHitIDs = trackInfo[1][0]
                for hIndex, thisHitID in enumerate(thisTrackHitIDs):
                    if hitID == thisHitID:
                        thisX2 = trackInfo[-1][hIndex]
                        if thisX2 < minX2:
                            minX2 = thisX2
                        trackIDs.append((tIndex,thisX2,hIndex))
                        
            # remove hit from other tracsk
            for tIndex in trackIDs:
                if tIndex[1] > minX2:
                    fittedTracks[int(tIndex[0])][1][0].pop(tIndex[2])
                    fittedTracks[int(tIndex[0])][-1]  .pop(tIndex[2])
        

    def removeOverlap(self,fittedTracks, hitIdsToClean, useNHits=True):
        if useNHits:
            self.removeOverlapNHits(fittedTracks,hitIdsToClean)
        else:
            self.removeOverlapX2(fittedTracks,hitIdsToClean)
                    

#print(recHits[0])
#recHits[:,0] = 70

## Testing 
#testMod = HTTrackFinding(0,2*np.pi,200,0,0.5,100)
##testMod.addHit(0 ,0.483, -0.129)
#
#testMod.addHit(0, 0, 0.4830312129294237 , -0.12915435469208822)
#testMod.addHit(1, 1, 0.9774524364399003 , -0.21115571149675902)
#testMod.addHit(2, 2, 1.479869952239391  , -0.2449181995258466 )
#testMod.addHit(3, 3, 1.986808451674113  , -0.2293298418355411 )
#testMod.addHit(4, 4, 2.4946649901971027 , -0.16323782247043445)
#testMod.addHit(5, 5, 2.999655976366993  , -0.04543152480140565)
#testMod.addHit(0, 6, -0.4362849066302877, -0.24424471385600377)
#testMod.addHit(1, 7, -0.9166316035243633, -0.3997330401910183 )
#testMod.addHit(2, 8, -1.4284636290133237, -0.457702589665042  )
#testMod.addHit(3, 9, -1.9578487136991394, -0.4084463419674918 )
#testMod.addHit(4, 10, -2.488400477394192 , -0.24054742589426548)
#testMod.addHit(5, 11, -2.999391191081526 ,  0.06043577469136796)
#testMod.combineLayerMaps()
#
#testMod.findTracks()
#testMod.getSolutions()
