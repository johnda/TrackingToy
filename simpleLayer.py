import numpy as np
import matplotlib.pyplot as plt
from TrackHelper import getHitsCylinderXY
from TrackHelper import getHitsCylinder3D
from matplotlib.patches import Arc
from math import floor

class simpleLayer:
    
    def __init__(self,layerID,layerR,lengthZ=None):
        self.layerID  = layerID
        self.layerR   = layerR
        if not lengthZ == None: 
            self.lengthZ = lengthZ
            

    def drawDetectorZR(self,ax,detailed=False):

        xs = [-self.lengthZ,self.lengthZ]
        ys_up = [self.layerR,self.layerR]
        plt.plot(xs,ys_up,"k",linestyle="--")

        ys_down = [-self.layerR,-self.layerR]
        plt.plot(xs,ys_down,"k",linestyle="--")

    def drawDetectorXY(self,ax,detailed=False):
        self.drawLayer(ax,detailed)
                
    def drawLayer(self,ax, detailed=False):
        ax.add_patch(Arc((0, 0), 2*self.layerR, 2*self.layerR, theta1=0.0, theta2=360.0, edgecolor='k',linestyle="--"))
        #thisLayer = plt.Circle((0, 0), self.layerR, color='k', linestyle="--",fill=False)

    def drawTower(self,ax, detailed=False):
        theta1=self.towerPhiMin*180./np.pi
        theta2=self.towerPhiMax*180./np.pi
        ax.add_patch(Arc((0, 0), 2*self.layerR, 2*self.layerR, theta1=theta1, theta2=theta2, edgecolor='k',linestyle="--"))


        if detailed:
            for ssItr in range(self.nSS):
                ssPhiMin = self.towerPhiMin+ssItr*self.ssSizePhi
                ssPhiMax = self.towerPhiMin+(ssItr+1)*self.ssSizePhi
                ssPhiMin *= 180./np.pi
                ssPhiMax *= 180./np.pi

                color = "k" if (ssItr % 2 ==0 ) else "y"
                ax.add_patch(Arc((0, 0), 2*self.layerR, 2*self.layerR, theta1=ssPhiMin, theta2=ssPhiMax, edgecolor=color,linewidth=3))

    # Takes a list of SSIDs to be drawn
    def drawSSID(self,ax,ssID):
        for ssItr in range(self.nSS):
            if not ssItr == ssID : continue

            ssPhiMin = self.towerPhiMin+ssItr*self.ssSizePhi
            ssPhiMax = self.towerPhiMin+(ssItr+1)*self.ssSizePhi
            ssPhiMin *= 180./np.pi
            ssPhiMax *= 180./np.pi

            color = "r"
            ax.add_patch(Arc((0, 0), 2*self.layerR, 2*self.layerR, theta1=ssPhiMin, theta2=ssPhiMax, edgecolor=color, linewidth=5))
        
        
    def getHits(self,trk_xc,trk_yc,trk_phi,trk_q, tol=0.01,debug=False):
        if debug: print("getHits layer",self.layerID)
        x_sol, y_sol = getHitsCylinderXY(self.layerR, trk_xc,trk_yc,trk_phi,trk_q, tol,debug)
        
        return x_sol, y_sol, self.layerID, 0

        
    def getHits3D(self,trk_xc,trk_yc,trk_phi,trk_theta, trk_z,trk_q, tol=0.01,debug=False):
        if debug: print("getHits3D layer",self.layerID)
        
        x_sol, y_sol, z_sol, r_sol  = getHitsCylinder3D(self.layerR, trk_xc,trk_yc,trk_phi,trk_theta, trk_z,trk_q, tol,debug)
    
        # check if layer is actually hit
        if (z_sol > self.lengthZ) or (z_sol < -1*self.lengthZ):
            return None,None,None,None,None,None

        return x_sol, y_sol, z_sol, r_sol, self.layerID, 0



    # For the FTK-like processing
    #   Probably want to make these add tower to list of all towers
    def makeTower(self, phiCenter, phiWidth, nSS):
        self.towerPhiCenter = phiCenter
        if self.towerPhiCenter < 0: self.towerPhiCenter += 2*np.pi

        self.towerPhiWidth  = phiWidth
        self.towerPhiMin    = phiCenter - phiWidth/2
        self.towerPhiMax    = phiCenter + phiWidth/2
        if self.towerPhiMin < 0: self.towerPhiMin += 2*np.pi
        if self.towerPhiMax < 0: self.towerPhiMax += 2*np.pi
        
        
        self.nSS = nSS
        self.ssSizePhi = self.towerPhiWidth/self.nSS        


    def getSSID(self, hitX,hitY):
        hitPhi = np.arctan2(hitY, hitX)   
        if hitPhi < 0: hitPhi += 2*np.pi        

        localPhi = (hitPhi - self.towerPhiMin)
        if localPhi < -np.pi: localPhi += 2*np.pi

        # shouldnt happen
        if localPhi > self.towerPhiWidth: 
            print("simpleLayer::ERROR: localPhi (",round(localPhi,2),") bigger then towerPhiWidth (",round(self.towerPhiWidth,2),")", self.towerPhiMin,hitX,hitY)
            return -1
        if localPhi < 0:
            print("hitPhi ",hitPhi/np.pi,"pi", hitX,hitY,self.towerPhiMin, self.towerPhiMax)
            print("simpleLayer::ERROR: localPhi negative",localPhi, hitX,hitY,self.towerPhiMin)
            print(self.hitInTower(hitX,hitY))
            return -1
        
        return floor(localPhi/self.ssSizePhi)



    def hitInTower(self, hitX,hitY,debug=False):
        hitPhi = np.arctan2(hitY, hitX)   
        if hitPhi < 0: hitPhi += 2*np.pi
        
        aPhiDiff = abs(hitPhi-self.towerPhiCenter)
        if aPhiDiff > np.pi: aPhiDiff = 2*np.pi-aPhiDiff
        
        if aPhiDiff > self.towerPhiWidth/2: 
            return False

        return True


    def DOClear(self):
        self.ssidToHit = {}

    def DOWrite(self,hID,hSSID):
        
        if hSSID not in self.ssidToHit:
            self.ssidToHit[hSSID] = []

        self.ssidToHit[hSSID].append(hID)

    # Called after DOwrite
    def isSSIDFired(self,SSID):
        return (SSID in self.ssidToHit)
