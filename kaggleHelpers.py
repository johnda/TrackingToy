import numpy as np
from detectorGeo import detectorGeo

B_in_T    = 1.89
T_to_GeV2 = 1.97*10**(-16)
B         = B_in_T*T_to_GeV2
e         = np.sqrt(4*np.pi/137)
GeV_to_mm = (5.06*10**(12))



def procssParticles(parts):
    
    parts["pt"]  = np.sqrt(parts["px"]**2+parts["py"]**2)
    parts["rc"]  = parts["pt"]/(e*B)/GeV_to_mm

    parts["phi"] = np.arctan2(parts["py"],parts["px"])
    parts["phi"][(parts["phi"] <0)] = (parts["phi"] + 2*np.pi)

    parts["theta"] = np.arctan2(parts["pt"],parts["pz"])
    parts["theta"][(parts["theta"] <0)] = (parts["theta"] + 2*np.pi)

    parts["eta"] = -np.log(np.tan(parts["theta"]/2))


def makeKaggleDetector(detData):

    # 
    # Get the barrel Info
    # 
    
    # Barrel IDs
    barrelIDs = [(8,4,35),(13,4,0),(17,2,0)] # Format / (volumeID, nlayers) / cz of central module (for phi counting)
    
    layerDataBarrel_oneModulePerRing = []
    
    for bInfo in barrelIDs:
        volume_id = bInfo[0]
        for layItr in range(bInfo[1]):
            layer_id = 2*(layItr+1)
            layerDataBarrel_oneModulePerRing.append(
                detData[(detData["volume_id"] == volume_id) & (detData["layer_id"] == layer_id) & (detData["cz"] == bInfo[2])]
            )
    
    nModulesInPhiBarrel = []
    for ld in layerDataBarrel_oneModulePerRing:
        nModulesInPhiBarrel.append(len(ld))
    nModulesInPhiBarrel


    # 
    # Create the Barrel
    #
    layerDataBarrel = []
    
    
    for bInfo in barrelIDs:
        volume_id = bInfo[0]
        for layItr in range(bInfo[1]):
            layer_id = 2*(layItr+1)
            layerDataBarrel.append(
                detData[(detData["volume_id"] == volume_id) & (detData["layer_id"] == layer_id)]
            )



    
    # Endcap IDs
    # Format / (volumeID, ndisks, nLayersR) / cz of central module (for phi counting)
    endcapIDs = [(7,7,2),(9,7,2),
                 (12,6,3),(14,6,3),
                 (16,6,2),(18,6,2),
                ]
    layerDataEndcap = []
    nModulesInREndcap = []
    for eInfo in endcapIDs:
        volume_id = eInfo[0]
        for diskItr in range(eInfo[1]):
            disk_id = 2*(diskItr+1)
            layerDataEndcap.append(
                detData[(detData["volume_id"] == volume_id) & (detData["layer_id"] == disk_id)]
            )
            nModulesInREndcap.append(eInfo[2])


    #
    #  Make the detector
    #
    detGeo = detectorGeo()
    detGeo.initFromFile(layerDataBarrel,nModulesInPhiBarrel,layerDataEndcap,nModulesInREndcap)
    return detGeo
